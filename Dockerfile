ARG TAG=latest
FROM ubuntu:${TAG}

RUN apt update \
 && apt install --no-install-recommends -y linux-image-generic \
 && apt install --no-install-recommends -y systemd \
 && apt install --no-install-recommends -y udev \
 && rm -rf /var/lib/apt/lists/*

RUN cd /boot; ln -s vmlinuz-* wyrcan.kernel
RUN ln -s /bin/systemd /init
